---
layout: post
title: Herbstschule Computerlinguistik in Passau
tags: [cl_fall_school]
lang: de
---

Die Computational Linguistics Fall School ist eine alle zwei Jahre stattfindende Veranstaltung für Masterstudierende und junge Doktoranden der Geisteswissenschaften und der Informatik, die ihr Wissen über die Computerlinguistik (CL) und die natürliche Sprachverarbeitung (NLP) erweitern möchten. Der Schwerpunkt liegt auf dem Angebot von Lehrveranstaltungen, die nicht traditionell in Regelstudiengängen angeboten werden.

Die CL Fall School wird von der GSCL gemeinsam mit der DGfS-CL unterstützt / organisiert.

Weitere Infos hier:
[https://cl-fallschool-2024.github.io/](https://cl-fallschool-2024.github.io/)