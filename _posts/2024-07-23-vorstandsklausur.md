---
layout: post
title: Vorstandsklausurtagung 2024 in Augsburg
tags:
lang: de
thumbnail: "assets/img/events/klausurtagung2024.JPG"
---

Vom 22.-23. Juli 2024 trafen sich der GSCL-Vorstand (1. Vorsitzende Annemarie Friedrich, 2. Vorsitzende Margot Mieskes, Informationsreferent Michael Spranger, Schatzmeisterin Gertrud Faaß, JLCL-Herausgeber Christian Wartena, Schriftführer Josef Ruppenhofer) und der Sprecher des GSCL-Beirats, Torsten Zesch, zu einer Klausurtagung an der Universität Augsburg. Wir freuten uns besonders über die beratende Teilnahme von Jonathan Baum, der die Interessen der Studierenden vertrat. Am zweite Tag fanden hybride Treffen statt, an denen auch die Sprecherin der DGfS-CL, Annette Hautli-Janisz, GSCL-Beiratsmitglied Nicolai Erbs sowie unser GSCL-Mitglied Jakob Prange teilnahmen. Auf der Tagesordnung standen der Entwurf neuer Förderformate und damit des Budgets 2025, eine Überarbeitung des Portal Computerlinguistik, sowie viele weitere Themen.

<img src="{{ '/assets/img/events/klausurtagung2024.JPG' | relative_url }}"  alt="Gruppenfoto der Teilnehmer*innen der Vorstandsklausur">