---
layout: post
title: Save the date - TaCoS 2025
tags: [tacos2025]
lang: de
---

Vom **22. bis 24. Mai 2025** findet die Tagung der Computerlinguistik-Studierenden [**TaCoS 2025**](https://tacosconference.github.io/) an der Ruhr-Universität **Bochum** statt. Im Mittelpunkt dieser Konferenz stehen Vorträge, Workshops und Poster-Sessions von Studierenden für Studierende. Darüber hinaus sind Vorträge etablierter Wissenschaftler geplant. Eine tolle Möglichkeit für Studierende sich zu vernetzen und wissenschaftlich auszutauschen.

Eine Registrierung ist bis zum 20.April 2025 [hier](https://tacosconference.github.io/register) möglich.

**Bitte teilt diese Informationen** über passende Kanäle, z.B. Newsletter oder E-Mail-Verteiler, mit potentiellen Interessenten!


<!--<div style="float:left; width: 280px; min-height: 500px; margin-left: 10px">
    <img src="{{ '/assets/img/events/teach4nlp2023_01.jpg' | relative_url }}">
 </div>-->
<!--<div style="float:left; width: 480px; min-height: 270px; margin-left: 10px">
    <img src="{{ '/assets/img/events/konvens2024promo.png' | relative_url }}">
</div>-->
<!--<div style="float:left; width: 480px; min-height: 270px; margin-left: 10px">
    <img src="{{ '/assets/img/events/konvens24promo.jpg' | relative_url }}">
</div>-->
<!--<div style="float:left; width: auto; min-height: auto; margin-left: 10px">
    <img src="{{ '/assets/img/events/konvens24promo.png' | relative_url }}">
</div>-->


 
<!--   <div style="float:left; width: 280px; min-height: 500px; margin-left: 10px">
    <img src="{{ '/assets/img/events/teach4nlp2023_02.jpg' | relative_url }}">
 </div>
  <div style="float:left; width: 280px; min-height: 500px; margin-left: 10px">
    <img src="{{ '/assets/img/events/teach4nlp2023_03.jpg' | relative_url }}">
 </div>
  <div style="float:left; width: 280px; min-height: 500px; margin-left: 10px">
    <img src="{{ '/assets/img/events/teach4nlp2023_04.jpg' | relative_url }}">
 </div>
 --> 
 <div style="clear:both;"></div>
