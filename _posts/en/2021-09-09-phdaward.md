---
layout: post
title: PhD Award
tags: [award]
lang: en
---

Every two years, the GSCL awards two prizes, each worth 400 Euros, for the best Bachelor thesis and for the best Master thesis in the field of language technology and computational linguistics. The winner in the category Bachelor for the selection round 2019–2021 is Yannic Bracke (University of Potsdam) for his work and presentation on the topic “Automatic text classification with imbalanced data – Building a frame classifier from a corpus of editorials”. In the Master's category, Marie Bexte (University of Duisburg-Essen) was awarded the prize for her work entitled “Combined Analysis of Image and Text Using Visio-Linguistic Neural Models – A Case Study on Robustness Within an Educational Scoring Task”. We warmly congratulate the winners on their excellent work.