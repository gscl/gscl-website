---
layout: post
title: DGFS-Workshop "Towards Linguistically Motivated Computational Models of Framing"
tags: [Workshop]
lang: en
feature-img: "assets/img/events/framing_2024_01.jpg"
thumbnail: "assets/img/events/framing_2024_01.jpg"
---

The workshop "Towards Linguistically Motivated Computational Models of Framing", organized by Annette Hautli-Janisz (U-Passau), Ines Rehbein (U-Mannheim), and Gabriella Lapesa (GESIS and U-Dusseldorf) took place on February 28/29 as a working group within the 46th Annual Conference of the German Linguistic Society (DGfS 2024).
The workshop featured 8 presentations (peer-reviewed abstracts) and 2 invited talks (sponsored by GSCL): Manfred Stede (U-Potsdam) on "Topics and Rhetorics - A multi-level approach to framing" and Elena Musi (U-Liverpool) on "Automatic Detection of Argumentative Frames: A Frame Semantics Approach". We concluded with an extremely fun shared annotation session: participants annotated three texts based on their own definitions/categories for framing, and we summarized the different views on the fly during the discussion, ending up with an extremely interesting multi-layered analysis.

Overall, the workshop was extremely successful in bringing together scholars working on framing from a very broad range of perspectives and methods, and the audience contributed with lively and fun discussions. Thanks to everyone!




 <div style="float:left; width: auto; min-height: auto; margin-left: 10px">
    <img src="{{ '/assets/img/events/framing_2024_03.jpg' | relative_url }}">
 </div>
  <div style="float:left; width: auto; min-height: auto; margin-left: 10px">
    <img src="{{ '/assets/img/events/framing_2024_02.jpg' | relative_url }}">
 </div>
  <div style="float:left; width: auto; min-height: auto; margin-left: 10px">
    <img src="{{ '/assets/img/events/framing_2024_01.jpg' | relative_url }}">
 </div>

 <div style="clear:both;"></div>

