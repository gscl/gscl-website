---
layout: post
title: KONVENS Vienna and GSCL PhD Award
tags:
lang: en
thumbnail: "assets/img/news/gscl-preis-2024-thumb.jpg"
---

This year's KONVENS took place in Vienna from September 10 to 13, 2024. Keynotes were given by Leonie Weißweiler (UT Austin), Sebastian Schuster (UC London) and Jana Diesner (TU Munich). As part of KONVENS, the GSCL offered financial support for the PhD dinner, at which 21 doctoral students networked and exchanged ideas, as well as for the two workshops CPSS and LIMO.

In addition, the GSCL PhD Prize in memory of Wolfgang Hoeppner is traditionally awarded as part of KONVENS. The winner for the period May 2022 - May 2024 is Dr. Enrica Troiano, who received her doctorate from the University of Stuttgart on the topic of “Where are emotions in text? A human-based and computational investigation of emotion recognition and generation”.

<img src="{{ '/assets/img/news/gscl-preis-2024.jpg' | relative_url }}"  alt="Foto der Preisträgerin mit den beiden Vorsitzenden der GSCL">