---
layout: post
title: GSCL Board Meeting 2024 in Augsburg
tags:
lang: en
thumbnail: "assets/img/events/klausurtagung2024.JPG"
---

From July 22-23, 2024, the GSCL board (President Annemarie Friedrich, Vice President Margot Mieskes, Information Officer Michael Spranger, Treasurer Gertrud Faaß, JLCL Editor Christian Wartena, Secretary Josef Ruppenhofer) and the spokesperson of the GSCL advisory board, Torsten Zesch, met at the University of Augsburg. We were particularly pleased about the advisory participation of Jonathan Baum, who represented the interests of our student members. On the second day, hybrid meetings were held, which were also attended by the spokesperson of the DGfS-CL, Annette Hautli-Janisz, GSCL advisory board member Nicolai Erbs and our GSCL member Jakob Prange. The agenda included the drafting of new funding formats and thus the 2025 budget, a revision of the Portal Computerlinguistik, and many other topics.


<img src="{{ '/assets/img/events/klausurtagung2024.JPG' | relative_url }}" alt="Group photo of the participants of the board meeting">
