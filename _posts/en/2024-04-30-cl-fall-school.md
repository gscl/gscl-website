---
layout: post
title: Computational Linguistics Fall School in Passau
tags: [cl_fall_school]
lang: en
---

The Computational Linguistics Fall School is a biennial event for master's students and young doctoral students in the humanities and computer science who want to expand their knowledge of computational linguistics (CL) and natural language processing (NLP). The focus is on offering courses that are not traditionally offered in regular degree programs.

The CL Fall School is supported/organized by the GSCL together with the DGfS-CL.

Further information here:
[https://cl-fallschool-2024.github.io/](https://cl-fallschool-2024.github.io/)