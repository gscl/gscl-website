---
layout: post
title: New funding type - GSCL MicroGrants
tags:
lang: en
---

Attention please - in 2025, the GSCL will for the first time fund up to two small projects by young scientists.
More information is available [here](/activities/microgrants/).