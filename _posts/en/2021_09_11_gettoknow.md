---
layout: post
title: GSCL Get2Know
feature-img: "assets/img/portfolio/ice-300x225.jpg"
img: "assets/img/portfolio/ice-1200x.jpg"
date: September 11, 2021
tags: [resources]
lang: en
---
# Ice Breaker Materials for GSCL Get2Know

### Preparation

If you like to, you can share your CVs etc. beforehand – but it's really not required, you can also just meet without any preparation!

### Step 1: If you were me ...

Take turns briefly introducing you to each other. However, don't do it the usual way, but instead change perspective. For example, if I'd introduce myself, I'd say: "If you were Annemarie Friedrich, you'd grow up as the oldest of four siblings in a small village near the Black Forest. Your main hobby is music .... Later, you move to Saarland to study computational linguistics because ..." etc.

### Schritt 2: Collect questions

As the time of your meeting is likely limited, why don't you collect a list of questions or points that you'd like to discuss.

### Schritt 3: Chat :)

### Schritt 4: Fill out feedback form (Link sent via e-mail)