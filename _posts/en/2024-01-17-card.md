---
layout: post
title: GSCL New Year's Greetings
tags: [konvens2024]
lang: en
feature-img: "assets/img/feature-img/card2024.jpg"
thumbnail: "assets/img/feature-img/card2024_thumb.jpg"
---
The GSCL wishes all members all the best for 2024.

* This year KONVENS invites you from the 9th to the 13th September 2024 to Vienna. We are looking forward to a personal exchange there again. At this point we would like to thank the KONVENS 2023 organizational team once again, who organized the first face-to-face conference in Ingolstadt after the Covid break, during which there were only online or hybrid events.
* Interesting for all students: The Computational Linguistics Fall School (organized jointly with the DGfS-CL) will take place from June 16th to 27th September 2024 in Passau.
* We would like to thank our creative member Eugenia Rykova (stage name Remolacha) for the beautiful design of the New Year's greeting card.


<!--<div style="float:left; width: 280px; min-height: 500px; margin-left: 10px">
    <img src="{{ '/assets/img/events/teach4nlp2023_01.jpg' | relative_url }}">
 </div>-->
<!--<div style="float:left; width: 480px; min-height: 270px; margin-left: 10px">
    <img src="{{ '/assets/img/events/konvens2024promo.png' | relative_url }}">
</div>-->
<!--<div style="float:left; width: 480px; min-height: 270px; margin-left: 10px">
    <img src="{{ '/assets/img/events/konvens24promo.jpg' | relative_url }}">
</div>-->
<div style="float:left; width: auto; min-height: auto; margin-left: 10px">
    <img src="{{ '/assets/img/news/card2024.jpg' | relative_url }}" alt="card image, Copyright Remolacha / Eugenia Rykova"><br/>
    Copyright Remolacha / Eugenia Rykova
</div>


 
<!--   <div style="float:left; width: 280px; min-height: 500px; margin-left: 10px">
    <img src="{{ '/assets/img/events/teach4nlp2023_02.jpg' | relative_url }}">
 </div>
  <div style="float:left; width: 280px; min-height: 500px; margin-left: 10px">
    <img src="{{ '/assets/img/events/teach4nlp2023_03.jpg' | relative_url }}">
 </div>
  <div style="float:left; width: 280px; min-height: 500px; margin-left: 10px">
    <img src="{{ '/assets/img/events/teach4nlp2023_04.jpg' | relative_url }}">
 </div>
 --> 
 <div style="clear:both;"></div>
