---
layout: post
title: Meeting of the Executive Board and Advisory Board in Fulda 2022
feature-img: "assets/img/news/vb2022_fulda.JPG"
thumbnail: "assets/img/news/vb2022_fulda_thumb.JPG"
tags: [meetings]
lang: en
---

After 2 years of virtual cooperation, on September 2 and September 3, a joint workshop of the executive board and the advisory board of the GSCL took place in Fulda.
The focus was on several working sessions, in which, among other things, the setup of the GSCL working groups, new concepts for the KONVENS conference, communication inside and outside the GSCL, ideas for the involvement of student members, as well as the redesigned GSCL website and plans for a redesign of the Portal Computational Linguistics were discussed.
Christian Wartena, who has been Editor since 2021, led a discussion on the future of the GSCL published journal JLCL.
The program was rounded off by a visit to the cathedral and a joint dinner.

<div style="float:left; width: 320px; min-height: 300px; margin-left: 10px">
    <img src="{{ '/assets/img/news/vb2022_fulda_1.JPG' | relative_url }}">
    Nicolai Erbs is presenting the new website.
 </div>
 <div style="float:left; width: 400px; min-height: 500px; margin-left: 10px">
    <img src="{{ '/assets/img/news/vb2022_fulda_2.JPG' | relative_url }}">
    Conference room with GSCL executive and advisory board members at the Brauhaus Wiesenmühle in Fulda.
 </div>
  <div style="float:left; width: 280px; min-height: 500px; margin-left: 10px">
    <img src="{{ '/assets/img/news/vb2022_fulda_3.JPG' | relative_url }}">
    Meta-plan board.
 </div>
 
 <div style="clear:both;"></div>
