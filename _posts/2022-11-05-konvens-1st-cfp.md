---
layout: post
title: Ankündigung KONVENS 2023 in Ingolstadt
tags: [conference]
feature-img: "assets/img/feature-img/konvens2023.jpg"
thumbnail: "assets/img/thumbnails/feature-img/konvens2023.jpg"
lang: de
---

# Ankündigung: KONVENS 2023 in Ingolstadt, September 2023

Die KONVENS 2023 findet vom 18. bis 22. September 2023 an der Technischen Hochschule Ingolstadt statt. Neben dem Fachprogramm bietet KONVENS einen regen Austausch zwischen akademischen Forschern und Kollegen aus der Industrie sowie Workshops, Tutorials, Shared Tasks und Networking-Events.
Wir laden insbesondere unsere studentischen und promovierenden Mitglieder ein, an der Veranstaltung teilzunehmen – eSie können z.B. ein Abstract zu einem aktuellen Projekt oder zu Ihrem Promotionsthema einreichen.
Deadline für die Einreichung von Papers und Abstracts ist der 19. Mai 2023.

Wir suchen auch Vorschläge für Tutorien, Workshops und gemeinsame Aufgaben - die Frist für Vorschläge ist der 30. November 2022.

[Image Credit](https://commons.wikimedia.org/wiki/File:Ingolstadt_Altes_Rathaus_2012_02.jpg) [Image License](https://creativecommons.org/licenses/by-sa/3.0/deed.en)
