---
layout: post
title: DGFS-Workshop "Towards Linguistically Motivated Computational Models of Framing"
tags: [Workshop]
lang: de
feature-img: "assets/img/events/framing_2024_01.jpg"
thumbnail: "assets/img/events/framing_2024_01.jpg"
---

Der Workshop "Towards Linguistically Motivated Computational Models of Framing", organisiert von Annette Hautli-Janisz (U-Passau), Ines Rehbein (U-Mannheim) und Gabriella Lapesa (GESIS und U-Dusseldorf), fand am 28. und 29. Februar als Arbeitsgruppe im Rahmen der 46. Jahrestagung der Deutschen Gesellschaft für Sprachwissenschaft (DGfS 2024) statt.
Der Workshop umfasste 8 Präsentationen (peer-reviewed abstracts) und 2 eingeladene (von der GSCL gesponserte) Vorträge: Manfred Stede (U-Potsdam) über "Topics and Rhetorics - A multi-level approach to framing" und Elena Musi (U-Liverpool) über "Automatic Detection of Argumentative Frames: A Frame Semantics Approach".
Wir beendeten den Workshop mit einer sehr unterhaltsamen gemeinsamen Annotationssitzung: Die Teilnehmenden annotierten Framing in drei Texten auf der Grundlage ihrer eigenen Definitionen/Kategorien. In der anschießenden Diskussion fassten wir die verschiedenen Ansichten zusammen und erhielten als Resultat eine äußerst interessante und vielschichtige Analyse.

Insgesamt war der Workshop ein großer Erfolg, da er Forschende zusammenbrachte, die sich mit Framing aus den verschiedensten Blickwinkeln und mit unterschiedlichen Methoden befassen, und das Publikum trug mit lebhaften und unterhaltsamen Diskussionen dazu bei. Vielen Dank an alle!



 <div style="float:left; width: auto; min-height: auto; margin-left: 10px">
    <img src="{{ '/assets/img/events/framing_2024_03.jpg' | relative_url }}">
 </div>
  <div style="float:left; width: auto; min-height: auto; margin-left: 10px">
    <img src="{{ '/assets/img/events/framing_2024_02.jpg' | relative_url }}">
 </div>
  <div style="float:left; width: auto; min-height: auto; margin-left: 10px">
    <img src="{{ '/assets/img/events/framing_2024_01.jpg' | relative_url }}">
 </div>

 <div style="clear:both;"></div>

