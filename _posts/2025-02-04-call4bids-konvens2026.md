---
layout: post
title: Call for Bids - KONVENS 2026
tags: [konvens2026]
lang: de
---

### Heute schon an die Konvens 2026 denken!

In diesem Sinne bitten wir um die Einreichung von Absichtserklärungen zur **Ausrichtung der KONVENS Tagung 2026**.

Die KONVENS (Konferenz zur Verarbeitung natürlicher Sprache) ist eine jährlich stattfindende wissenschaftliche Tagung zu allen Themen aus den Bereichen Computerlinguistik und Sprachtechnologie.
Die KONVENS wird von verschiedenen Fachgesellschaften (DGfS-CL, GSCL, ASAI, SwissNLP) aus dem deutschsprachigen Raum ausgerichtet und findet üblicherweise im Herbst statt.
Eine Liste der bisherigen KONVENS Tagungen findet sich [hier](https://konvens.org/) oder [hier](https://dgfs.de/de/cl/konvens.html).


Im Rahmen der KONVENS finden üblicherweise auch Workshops, Tutorials, und die [GermEval Shared Tasks](https://germeval.github.io/ ) statt.

Die Deadline für die Einreichung von Absichtserklärungen ist der **31.6.2025**.
Eine Absichtserklärung sollte folgende Punkte umfassen:
    - Persönliche Daten der OrganisatorInnen
    - Ort der Ausrichtung
    - Thematischer Schwerpunkt (wenn angestrebt)
    - Mögliche Termine der Tagung (Vermeidung von Feiertagen, Messen / Kongressen)
    - Beschreibung der anvisierten Räume und welche Kosten für die Anmietung entstehen
    - Finanzielle und organisatorische Unterstützung vor Ort
Die Fachgesellschaften bewerten gemeinsam die eingegangenen Einreichungen und wählen aus diesen aus. Dabei werden auch regionale Gesichtspunkte (z.B. wo wurde lange nicht organisiert) und der thematische Hintergrund der OrganisatorInnen berücksichtigt.
Fragen oder Absichtserklärungen richten Sie bitte an die [Vorsitzende der GSCL](mailto:vorsitzende@gscl.org).

