---
layout: post
title: Stimmen von der Konvens 2024
tags: [konvens2024]
lang: de
thumbnail: "assets/img/events/konvens_voices_thumb.jpg"
---

Vom 09. bis 13. September 2024 fand die KONVENS 2024 in Wien statt, hervorragend organisiert von der [ASAI](https://www.asai.ac.at/). Die GSCL förderte die Teilnahme von drei Doktorandinnen durch Vergabe von Konferenz-Stipendien. Nachfolgend berichten die Begünstigten ihre Eindrücke von der Veranstaltung:

**Pia Pachinger**

Die Konvens 2024 fand vom 10. bis 13. September 2024 in Wien statt und widmete sich verschiedenen Aspekten des Natural Language Processing (NLP). Organisiert wurde die Konferenz von Brigitte Krenn, Dagmar Gromann, Barbara Heinisch, Michael Wiegand, 
Pedro Henrique Luz de Araujo, Benjamin Roth und Andreas Baumann. Sie richtete sich an Forschende, Entwicklende und Interessierte im Bereich der Verarbeitung natürlicher Sprache. Zu den Schwerpunktthemen der Konferenz gehörten unter anderem Semantik und Diskurs, Informationsextraktion und Korpora, Textverstehen, Sprache und Gesellschaft, Features und Repräsentationen, und Sentiment, multilinguale Methoden und gesprochene Sprache. Besonders hervorzuheben ist der Workshop "GERMS-Detect", der sich intensiv mit der Erkennung von Sexismus in deutschen Userkommentaren auseinandersetzte.

Der GERMS-Detect-Workshop bot einen tiefgehenden Einblick in aktuelle Ansätze zur automatisierten Erkennung von sexistischen Inhalten in Texten. Zunächst wurden die verwendeten Datensätze von den Organisierenden präsentiert, was eine fundierte Grundlage für die darauffolgenden Präsentationen der verschiedenen Ansätze zur Detektion bot. Die Vielfalt der vorgestellten Methoden war besonders bemerkenswert, die Teams nutzten unterschiedlichste Modelle und Herangehensweisen, den Multitask zu modellieren – wobei die meisten Bert-basierte Ansätze testeten. Die Heterogenität der Ansätze zeigte deutlich, dass es im Bereich der automatisierten Erkennung von sexistischen Inhalten noch viele offene Forschungsfragen und Optimierungspotenziale gibt und die vorgestellten Ergebnisse bieten eine solide Grundlage für künftige Arbeiten.

Im Anschluss an die Präsentationen fand eine technisch fundierte Diskussion über die Zukunft der annotierten Daten und mögliche Forschungsrichtungen statt. Dabei wurde insbesondere auf die Qualität und Vielfalt der Daten sowie auf die Notwendigkeit gemeinsamer Standards eingegangen, um die Vergleichbarkeit und Übertragbarkeit von Modellen zu gewährleisten. Am Ende des Workshops gab es eine öffentlich zugängliche Podiumsdiskussion zum Thema "KI in der Medienproduktion", in welcher eine ein für Österreich repräsentatives Spektrum an Stakeholdern teilnahm. 

Der GERMS-Detect-Workshop auf der Konvens 2024 hat richtungsweisende Diskussionen angestoßen und zeigte auf, dass die Erkennung von Sexismus in Texten ein dynamisches und vielschichtiges Forschungsfeld ist. Die Veranstaltung war von offenem Austausch und einer Vielfalt an Ansätzen geprägt und bietet eine vielversprechende Basis für zukünftige Entwicklungen. 

**Jenny Felser**

Dank des Konferenzstipendiums der Gesellschaft für Sprachtechnologie und Computerlinguistik (GSCL) hatte ich die Möglichkeit, an der KONVENS 2024 in Wien teilzunehmen. Als Doktorandin im ersten Jahr meiner Promotion im Bereich des forensischen Text Minings war der Besuch der Konferenz für mich die ideale Gelegenheit, Inspiration für meine eigene Forschung zu erhalten und neue Arbeiten in meinem Forschungsgebiet kennenzulernen. 
Sehr interessant waren die Keynotes zu Themen wie Herausforderungen für Large Language Models (LLMs) beim Verstehen und Generieren von Texten. Darüber hinaus boten die Poster Sessions eine gute Gelegenheit, mit den Autorinnen und Autoren in Kontakt zu treten und intensive Diskussionen über eine Vielzahl interessanter Forschungsthemen zu führen, die von der Erstellung neuer Datensätze zur Textklassifikation über die Extraktion von Schlüsselwörtern bis hin zur Anwendung von LLMs z.B. in der maschinellen Übersetzung reichten. Insbesondere die (Poster-)Vorträge zu Themenmodellierung und Forschungsthemen aus dem Bereich des forensischen Text Mining werden meine eigenen Forschungsansätze in diesem Bereich bereichern und ich freue mich schon darauf, diese auszuprobieren. 
Neben den Vorträgen haben mir auch die Social Events der KONVENS 2024 sehr gut gefallen. So bot der GSCL Networking Lunch eine hervorragende Möglichkeit, Kontakte zu knüpfen und sich in entspannter Atmosphäre auszutauschen. Auch der Besuch der Aussichtsplattform am Cobenzl mit anschließendem Spaziergang durch die Weinberge und gemeinsamem Konferenzdinner war ein schönes Erlebnis. Ein besonderes Highlight war das von der GSCL gesponserte PhD Dinner im Gasthaus Hansy. Es war sehr bereichernd, mit anderen Doktoranden ins Gespräch zu kommen und sich über die verschiedenen Forschungsarbeiten, Erfahrungen und Herausforderungen der Promotionszeit auszutauschen. 
Zusammenfassend kann ich sagen, dass die Teilnahme an der KONVENS 2024 ein voller Erfolg war und für meine wissenschaftliche Entwicklung von großem Nutzen ist.


In September 2024, I attended the Conference on Natural Language Processing (KONVENS) in Vienna. It was my second time at KONVENS and also a special one, as it was the last scientific event I attended in person before a break for maternity leave. 
Each submission had been reviewed by three reviewers, and I was happy to receive their comments. It helped me to add some theoretical perspectives and make my paper better overall. In total, 68% of the submissions had been accepted, and my paper  „Linguistic and extralinguistic factors in automatic speech recognition of German atypical speech“ was one of the 21 long ones. I presented it as a poster on the second day of the main conference.

**Eugenia Rykova**

<img src="{{ '/assets/img/events/konvens_voices_06.jpg' | relative_url }}"  alt="Eugenia Rykova">

Leonie Weissweiler, UT Austin, opened the academic programme of the main conference with her talk on compositionality in LLMs. I found it particularly interesting how LLMs overgeneralise grammar rules when prompted with pseudowords. That reminded me of children’s hypercorrective grammars, on the one hand, and of my own mistakes in German as a foreign language, on the other. The author has not compared the LLMs‘ performance to children or adult language learners, so it could be a curious point to team up in the future as I work with language acquisition data.

<img src="{{ '/assets/img/events/konvens_voices_05.jpg' | relative_url }}"  alt="Keynote Leonie Weissweiler, UT Austin">

One hour was not enough for the first poster session (just like for the others), so I only managed to listen to two colleagues. The poster of Dana Neumann, Ruhr-Universität Bochum, encouraged interesting discussion and ideas of further applications of the presented methods in the field of atypical language development research. I also discussed GermaNet peculiarities and possible applications in out-of-research contexts with Claus Zinn, University of Tübingen.

<img src="{{ '/assets/img/events/konvens_voices_04.jpg' | relative_url }}"  alt="Gspräch mit Claus Zinn">

In the afternoon, I had a chance to enjoy GSCL-DGfs-CL Networking Lunch. It was especially important for me to engage with senior female researchers, who shared their experiences of family-and-work balance.
The keynote by Sebastian Schuster, University College London, left an impression that on the one hand, LLMs‘ understanding abilities are still far from those of humans. However, the author underlined the importance of task design and presented ideas on how to overcome some challenges in the evaluation of LLMs‘ understanding abilities. 
After two more sessions, conference participants and guests were taken to a viewing platform in the north of the city. Perfectly timed, it was the last day of good weather, so we enjoyed the views of Vienna and a walk through the vineyards – and typical Austrian food and folk music afterward.

<img src="{{ '/assets/img/events/konvens_voices_03.jpg' | relative_url }}"  alt="Impression von den Weinbergen">

I had been slightly disappointed that my paper was to be presented as a poster, but it worked out perfectly. I had a chance to talk about my research beyond the scope of the paper, go into details, answer an unlimited amount of questions, and discuss some prospective collaborations. I am looking forward to getting back to work after the break!
At the end of the main programme, Enrica Troiano, HK3 Lab, presented her PhD thesis on text emotion recognition and generation. The thesis was defended at the University of Stuttgart and awarded a GSCL PhD Award in memory of Wolfgang Hoeppner. The presentation opened interesting perspectives on human emotion perception and production, and the ways of applying the findings in computational approaches. It sparked a vivid discussion in the audience: no wonder since it was about emotions!

<img src="{{ '/assets/img/events/konvens_voices_02.jpg' | relative_url }}"  alt="Preisverleihung Phd-Award">

The PhD students got together for dinner after the closing ceremony. Among us, there were representatives of many universities, not only from German-speaking countries, and different fields as some of the participants had their presentations during the multidisciplinary workshops outside of the main conference programme. That evening was a pleasant end of my time at KONVENS 2024, which I overall enjoyed a lot.

<img src="{{ '/assets/img/events/konvens_voices_01.jpg' | relative_url }}"  alt="Gruppenfoto der Teilnehmer*innen des Phd-Dinners">