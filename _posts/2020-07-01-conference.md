---
layout: post
title: SwissText & KONVENS Joint Conference 2020
feature-img: "assets/img/feature-img/konvens2020.png"
thumbnail: "assets/img/thumbnails/feature-img/konvens2020.png"
tags: [conferences]
lang: de
---

Vom 23. bis 25. Juni 2020 fand die ursprünglich in Zürich geplante gemeinsame Konferenz SwissText und KONVENS [Link](https://swisstext-and-konvens-2020.org) online statt. SwissText wird seit 2016 von der Fachhochschule Zürich organisiert. KONVENS ist eine jährliche Konferenz zur Verarbeitung natürlicher Sprache, die normalerweise in Deutschland oder Österreich stattfindet und von der GSCL gesponsort wird. Das Konferenzprogramm umfasste die Präsentation von 38 Vorträgen oder Demos, vier Shared Tasks sowie Vorträge von Anya Belz, Roberto Navigli und Holger Schwenk. 132 Teilnehmer aus Wissenschaft, Politik und Industrie nahmen an der virtuellen Konferenz teil. Das GSCL KONVENS 2020 Online-Stipendium wurde an Ian Clotworthy von der Universität Potsdam vergeben.
