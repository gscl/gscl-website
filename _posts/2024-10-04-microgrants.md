---
layout: post
title: Neues Förderprogramm - GSCL MicroGrants
tags:
lang: de
---

Aufgepasst - im Jahr 2025 fördert die GSCL erstmals bis zu zwei Kleinprojekte von Nachwuchswissenschaftler*innen.
Mehr Infos dazu gibt es [hier](/activities/microgrants/).