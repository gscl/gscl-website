---
layout: page
title: Computerlinguistik für die Politik- und Sozialwissenschaften
feature-img: "assets/img/sigs/politics.jpg"
permalink: /activities/politicssocialsciences/
tags: [SIG]
lang: de
---
Leitung: [Goran Glavaš](mailto:goran@informatik.uni-mannheim.de), [Gabrielle Lapesa](mailto:gabriella.lapesa@ims.uni-stuttgart.de), [Simone Ponzetto](mailto:simone@informatik.uni-mannheim.de), [Ines Rehbein](mailto:ines@informatik.uni-mannheim.de)

Der 2021 gegründete Arbeitskreis möchte Forschende aus den Bereichen der Computerlinguistik/NLP und aus den Politik- und Sozialwissenschaften und der Text-as-Data Community zusammenbringen, um Ideen und Ergebnisse an der Schnittstelle beider Bereiche auszutauschen.

Mögliche Initiativen des neuen Arbeitskreises beinhalten die folgenden Themen:

* Erstellung von Ressourcen und Tools für die politische Textanalyse
* Erstellung von Benchmarking-Datensets fürs Deutsche für die politische Textanalyse
* Organisation von Workshops: CL für die Politik- und Sozialwissenschaft
* Organisation von Shared Tasks zu Themen relevant für die politische Textanalyse
* Stärkung der Zusammenarbeit zwischen der CL und der "Text-as-Data PolSci Community"


Wir sind natürlich auch offen für weitere Initiativen und Aktivitäten und freuen uns auf Eure Ideen!

Bild: CC-BY-4.0: © European Union 2019 – Source: EP, Creator: Gabor KOVACS

Workshops
* [Towards Linguistically Motivated Computational Models of Framing@DGFS-Jahrestagung 2024]({% post_url 2024-04-02-dgfs-workshop-framing %})
* [CPSS@KONVENS 2023 (Ingolstadt, 22 September 2023, sponsored by GSCL)]({% post_url 2024-03-19-Workshop-CPSS-2023 %})
* [CPSS@KONVENS 2022 (Potsdam, 12 September 2022)]({% post_url 2022-01-01-cpss_cfp_2022 %})
* [CPSS@KONVENS 2021 (online, 10 July 2021)]({% post_url 2021-01-01-cpss2021 %})





<!-- links outdated -->
<!-- * [CPSS@KONVENS 2022](/{{site.baseurl}}/2022/01/01/cpss2022.html)
* [CPSS@KONVENS 2021](/{{site.baseurl}}/2021/01/01/cpss2021.html)-->
