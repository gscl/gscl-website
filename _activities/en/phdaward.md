---
layout: page
title: GSCL Award for the best Doctoral Thesis in memory of Wolfgang Hoeppner
lang: en
feature-img: "assets/img/award.jpg"
tags: [award]
---


The German Society for Computational Linguistics (GSCL) awards a prize for an excellent doctoral thesis in the field of language technology / computational linguistics once every two years. The thesis must have been submitted in one of the disciplines or its subdisciplines.

## Competition 2024

Candidates should have defended their work (viva voce), but not earlier than May 2022. Prior nomination for other awards is permitted. Theses from all German-speaking countries are acceptable (Austria, Germany and Switzerland) as well as from any other country, as long as the topic is focused on the German language.

The following documentation must be submitted in the PDF format:

- a copy of the thesis,
- at least one recommendation (generally by the first referee),
- summary of the doctoral thesis (max. 10 pages),
- list of the candidate’s publications,
- curriculum vitae.

Work that marks relevant progress and can be expected to significantly advance language technology / computational linguistics will be considered for the award. The successful candidate will be awarded € 1000. Submission deadline for 2024: May 31, 2024, 23:59 CEST. All documents must be submitted by this date, either in German or English, via e-mail to [schneider@ids-mannheim.de](mailto:schneider@ids-mannheim.de). The prize will be awarded at KONVENS 2022. It is possible to increase the number of awardees, and the prize money can be shared. Recourse to the courts is ruled out.


## Awardees since 2014


### 2024
- Enrica Troiano (University of Stuttgart): [Where are emotions in text? A human-based and computational investigation of emotion recognition and generation](http://dx.doi.org/10.18419/opus-13652).
  - Advisors: Roman Klinger and Sebastian Padó

### 2022
- Anna Valer’evna Shadrov (Humboldt-Universität zu Berlin): [Measuring coselectional constraint in learner corpora: A graph-based approach](http://dx.doi.org/10.18452/21606).

    - Advisor: Anke Lüdeling

### 2020

- Thomas Proisl (Friedrich-Alexander-Universität Erlangen-Nürnberg): [The Cooccurrence of linguistic structures](http://doi.org/10.25593/978-3-96147-201-7).

  - Supervisor: Stefan Evert

- Seid Muhie Yimam (Universität Hamburg): [Adaptive Approaches to NLP in Annotation and Application](https://nbn-resolving.org/urn%3Anbn%3Ade%3Agbv%3A18-99140).

  - Supervisor: Chris Biemann

### 2018


Annemarie Friedrich (Universität des Saarlandes): ]States, events, and generics: computational modeling of situation entity types](https://publikationen.sulb.uni-saarland.de/bitstream/20.500.11880/23722/1/scidok_final.pdf)

Referees: Manfred Pinkal, Alexis Palmer

### 2016

Martin Riedl (Technische Universität Darmstadt): [Unsupervised Methods for Learning and Using Semantics of Natural Language](https://tuprints.ulb.tu-darmstadt.de/5435/7/Dissertation_Riedl_Unsupervised_Methods_Semantics.pdf)

Referees: Chris Biemann, Anders Søgaard

### 2014

-  Nadir Durrani (Stuttgart): [A Joint Translation Model with Integrated Reordering](http://dx.doi.org/10.18419/opus-2987)
-  Honorable Mention: Annelen Brunner (Würzburg): [Automatische Erkennung von Redewiedergabe in literarischen Texten](https://doi.org/10.1515/9783110417425)
