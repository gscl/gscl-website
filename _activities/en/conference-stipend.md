---
layout: page
title: GSCL-Conference Stipend for Students (KONVENS 2023)
lang: en
feature-img: "assets/img/award.jpg"
tags: [award]
---

# GSCL conference scholarships for Bachelor/Master/Doctoral students for the 2024 KONVENS in Vienna

The GSCL offers scholarships for students and doctoral students in computational linguistics and related subjects in the form of covering the participation fees and (partial) support of travel costs for visiting KONVENS 2024 in Vienna.

## Selection criteria/requirements
* Applicants must be enrolled in a computational linguistics course or a related subject (doctoral students: at least registered).
* Applicants must be a member of the GSCL (the application for membership should, if necessary, be submitted in parallel with the application for the scholarship).
* Students who have excellent academic results and show a high interest in research, or who are already active in GSCL, will be given preference.
* Your own contribution to the conference is not required, but it is a plus when applying.

## Scholarship amount
The scholarship covers the conference fee for KONVENS 2024 in Vienna. Reimbursement of travel and accommodation costs can also be applied for, but this is only possible if the overall budget is sufficient.

### Application modalities
Please send your application to schatzmeister@gscl.org by August 31, 2024 at the latest with the following information and attachments:
1. Name, course of study, university
2. Proof of enrollment (immatriculation)
3. Tabular CV
4. Proof of place of residence (copy of ID)
5. Description of your motivation: why do you want to take part in the conference, what do you expect from it (max. 300 words) or why did you take part in the conference and what did you gain from it (half-page report)?

The decision on the application will be made within two weeks after the application has been submitted.
If there is a need for partial reimbursement of travel and accommodation costs, we ask for justification and proof of the costs incurred. The decision on the corresponding reimbursement is made by the GSCL board; the application will of course be treated confidentially. However, the refund will not be made until December 2024, after all student funding applications have been taken into account.

Doctoral students commit to create (if possible together) a contribution for the GSCL website and other relevant sessions at this year's KONVENS. Of course, the authors of the article are named on the website. You are also welcome to submit photos for publication on the website. The article must be submitted to informationsreferent@gscl.org by October 15, 2024.
The scholarship should be mentioned in your CV as _GSCL Conference Scholarship 2024_.