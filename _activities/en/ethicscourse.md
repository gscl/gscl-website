---
layout: page
title: A Crash Course to Ethics in NLP
feature-img: "assets/img/sigs/ethics.jpg"
img: "assets/img/sigs/ethics.jpg"
permalink: /resources/ethics-crash-course
date: June 27, 2021
tags: [Ethics]
lang: en
---
<!-- Please ensure path https://gscl.org/resources/ethics-crash-course -- mentioned in paper! -->

This page describes a set of ready-made teaching materials for a crash course in ethics for natural language processing originating from joint GSCL working sessions. The materials are published under CC-BY and intended to be integrated into introductory NLP courses. If you are using the materials, please cite:

> Annemarie Friedrich and Torsten Zesch. 2021. [A Crash Course on Ethics for Natural Language Processing](https://aclanthology.org/2021.teachingnlp-1.6) . In Proceedings of the Fifth Workshop on Teaching NLP, pages 49–51, Online. Association for Computational Linguistics.

The Google slides can be found [here](https://docs.google.com/presentation/d/1xt1FFtz67zWgIKOYy2puarORSthBb_rj3SfOuikvr-8/edit?usp=sharing). We recommend accessing the Google slides for obtaining the most recent version of our slides.
However, in case you cannot access Google slides, we also provide [PPTX][1] and [PDF][2] versions for download.

If you have any suggestions, feel free to add your comments directly to the Google slides, but also please send us an [e-mail](mailto:annemarie.friedrich@gmail.com).


[1]:{{ site.url }}/assets/crash_course_ethics_v2.pptx
[2]:{{ site.url }}/assets/crash_course_ethics_v2.pdf