---
layout: page
title: GSCL MicroGrants 2025
lang: en
feature-img: "assets/img/award.jpg"
tags: [funding]
---

The GSCL will award up to two MicroGrants for (independent) projects related to computational linguistics or language technology in 2025, each with a maximum of 2500 euros. __The aim of the initiative is to give young researchers their first experience in raising third-party funding and carrying out their own research project.__ The projects should be carried out independently of any other third-party funding and ideally lead to an independent publication. The assessment is carried out by the GSCL board and advisory board. Additional reviewers can also be requested.
The funds are available for material costs (including student assistants), but not for personnel resources. The costs must be settled within 2025.

Young researchers who hold a doctoral researcher position at the time the funds are used, as well as postdocs up to a maximum of 3 years after defending their doctoral thesis at universities and colleges, are eligible to apply. Applications can only be submitted by GSCL members. Deadline for submitting the application: __31.12.2024__ (by email to vorsitzende@gscl.org)<br/>
Notification: 31.1.2025

If you have any questions, please contact vorsitzende@gscl.org.

Documents to be submitted:
* CV with a list of max. 6 of your own most important publications in relation to the proposed project (please only link to them in your CV, do not send PDFs)
* Description of the project (with the sections motivation, state of the art, research questions and planned approach, time and financial plan, publication strategy for articles, data and code), max. 5 pages (plus references) in [ACL format](https://github.com/acl-org/acl-style-files) (no appendix)

Review criteria:
* Motivation
* Originality of the methodology / data set
* Ethically and legally sound approach to data collection / annotation or similar
* Feasibility of the project