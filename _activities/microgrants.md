---
layout: page
title: GSCL MicroGrants 2025
lang: de
feature-img: "assets/img/award.jpg"
tags: [funding]
---


Die GSCL vergibt für das Jahr 2025 bis zu zwei MicroGrants für (eigenständige) Projekte mit Bezug zur Computerlinguistik oder Sprachtechnologie zu je maximal 2500 Euro. __Ziel der Initiative ist es, Nachwuchswissenschaftler:innen erste Erfahrungen im Einwerben von Drittmitteln sowie der Durchführung eines eigenen Forschungsprojekts zu ermöglichen.__ Die Projekte sollen unabhängig von weiteren Drittmitteln durchgeführt werden und idealerweise zu einer eigenständigen Publikation führen. Die Begutachtung erfolgt durch den Vorstand und den Beirat der GSCL. Zusätzlich können weitere Gutachter*innen angefragt werden.
Die Mittel stehen für Sachmittel zur Verfügung (inkl. Studentische Hilfskräfte), jedoch nicht für Personalmittel. Die Abrechnung der Kosten muss innerhalb des Jahres 2025 erfolgen.

Antragsberechtigt sind Nachwuchswissenschaftler:innen, die zum Zeitpunkt der Mittelverwendung eine Promotionsstelle innehaben, sowie Postdocs bis maximal 3 Jahre nach Verteidigung ihrer Doktorarbeit an Universitäten und Hochschulen. Anträge können nur von GSCL-Mitgliedern gestellt werden. Deadline für die Einreichung des Antrags: __31.12.2024__ (per Mail an vorsitzende@gscl.org)<br/>
Notification: 31.1.2025 

Für Rückfragen können Sie sich gern an vorsitzende@gscl.org wenden.

Einzureichende Unterlagen:
* Lebenslauf mit Liste der max. 6 wichtigsten eigenen Publikationen in Bezug auf das vorgeschlagene Projekt (bitte nur im Lebenslauf verlinken, keine PDFs mitschicken)
* Beschreibung des Vorhabens (mit den Abschnitten Motivation, Stand der Technik, Forschungsfragen(n) und geplante Vorgehensweise, Zeit-/und Finanzplan, Veröffentlichungsstrategie für Artikel, Daten, Code), max. 5 Seiten (plus Referenzen) im [ACL-Format](https://github.com/acl-org/acl-style-files) (kein Anhang)

Begutachtungskriterien:
* Motivation
* Originalität der Methodik / des Datensatzes
* Ethisch und rechtlich einwandfreies Vorgehen bei der Datensammlung/Annotation o.ä.
* Realisierbarkeit des Vorhabens
