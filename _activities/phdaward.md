---
layout: page
title: GSCL-Promotionspreis zum Gedenken an Wolfgang Hoeppner
lang: de
feature-img: "assets/img/award.jpg"
tags: [award]
---

Die GSCL vergibt alle zwei Jahre einen Preis für eine herausragende
Dissertation im Bereich der Sprachtechnologie bzw. Computerlinguistik.
Die Dissertation muss in einem Themengebiet des Faches bzw. seiner
Teilgebiete angesiedelt sein.

## Ausschreibung 2024

Bewerber sollen ihre Dissertation bereits verteidigt haben, allerdings darf die Verteidigung nicht vor Mai 2022 stattgefunden haben. Die Arbeit darf bereits für andere Preise nominiert worden sein. Teilnahmeberechtigt sind alle Dissertationen aus deutschsprachigen Ländern (Deutschland, Österreich und die Schweiz) oder aus anderen Ländern, sofern sie sich mit der deutschen Sprache als Untersuchungsgegenstand befassen.

Der Arbeit müssen folgende Unterlagen (im PDF-Dateiformat) beigefügt
sein:

-   die Dissertationsschrift,
-   mindestens ein Empfehlungsschreiben (in der Regel von der/dem
    Erstgutachter/in),
-   Zusammenfassung der Dissertation (max. 10 Seiten),
-   eine Publikationsliste der Doktorandin bzw. des Doktoranden,
-   ein Curriculum Vitae.

Preiswürdig sind Arbeiten, die einen großen Fortschritt für die
Sprachtechnologie und Computerlinguistik bedeuten und das Fach
signifikant voranbringen.

Der Preis ist mit 1000,- Euro dotiert. Einsendeschluss für den Promotionspreis 2024 ist der 31. Mai 2024, 23:59 Uhr MESZ. Alle Dokumente sind fristgerecht in deutscher oder englischer Sprache per E-Mail an den Sprecher des wissenschaftlichen Beirats der GSCL ([torsten.zesch@fernuni-hagen.de](mailto:torsten.zesch@fernuni-hagen.de)) zu senden. Die Preisverleihung soll im Rahmen der KONVENS 2024 stattfinden. Die Anzahl der Preisträger kann gegebenenfalls erweitert und der Preis geteilt werden. Der Rechtsweg ist ausgeschlossen.

![](https://gscl.org/media/pages/auszeichnungen/5589017-1615118697/phd.jpg)

## PreisträgerInnen seit 2014

### 2024
- Enrica Troiano (Universität Stuttgart): [Where are emotions in text? A human-based and computational investigation of emotion recognition and generation](http://dx.doi.org/10.18419/opus-13652).
  - Betreuer: Roman Klinger und Sebastian Padó


### 2022
- Anna Valer’evna Shadrov (Humboldt-Universität zu Berlin): [Measuring coselectional constraint in learner corpora: A graph-based approach](http://dx.doi.org/10.18452/21606).

    - Betreuerin: Anke Lüdeling

### 2020

- Thomas Proisl (Friedrich-Alexander-Universität Erlangen-Nürnberg): [The Cooccurrence of linguistic structures](http://doi.org/10.25593/978-3-96147-201-7).

  - Betreuer: Stefan Evert

- Seid Muhie Yimam (Universität Hamburg): [Adaptive Approaches to NLP in Annotation and Application](https://nbn-resolving.org/urn%3Anbn%3Ade%3Agbv%3A18-99140).

  - Betreuer: Chris Biemann

### 2018

Annemarie Friedrich (Universität des Saarlandes): [States, events, and generics: computational modeling of situation entity types](https://publikationen.sulb.uni-saarland.de/bitstream/20.500.11880/23722/1/scidok_final.pdf)

Referees: Manfred Pinkal, Alexis Palmer

### 2016

Martin Riedl (Technische Universität Darmstadt): [Unsupervised Methods for Learning and Using Semantics of Natural Language](https://tuprints.ulb.tu-darmstadt.de/5435/7/Dissertation_Riedl_Unsupervised_Methods_Semantics.pdf)

Referees: Chris Biemann, Anders Søgaard

### 2014

-   Nadir Durrani (Stuttgart): [A Joint Translation Model with
    Integrated Reordering](http://dx.doi.org/10.18419/opus-2987)
-   Honorable Mention: Annelen Brunner (Würzburg): [Automatische
    Erkennung von Redewiedergabe in literarischen Texten](https://doi.org/10.1515/9783110417425)
