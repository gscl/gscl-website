---
layout: page
title: GSCL Member FAQ
thumbnail: "assets/img/portfolio/faq-300x225.jpg"
img: "assets/img/portfolio/faq-300x225.jpg"
date: June 27, 2021
tags: [resources]
lang: en
---

This page is intended to cover many of the questions that new members may have. If you feel something is missing, or needs a better explanation, please [contact us](mailto:vorsitzende@gscl.org).
## How can I get in touch with other members, meet people with similar interests?
The main yearly networking events sponsored by the GSCL are KONVENS (for everybody) and TaCoS (for students). These are usually taking place in some German, Swiss, or Austrian city and last for around 3 days. If you are interested in meeting people with similar interest, consider joining or opening a special interest group. These groups are dynamic, may exist for several years or shorter, depending on the members' interests. Besides the monthly research and tutorial talks, we irregularly (currently virtual) hold workshops on particular topics or meetups.
## What are the best ways to get in touch, ask a question, look for internships etc.?
The main communication channel for members is our mailing list, to which you are automatically added if you join the GSCL. Posts include information by the GSCL committees, news, workshop invites, calls for papers, job announcements, etc. Feel free to post any questions you have on this mailing list, too. Our LinkedIn community is growing, so hopefully at some point, this will also be a somewhat more informal communication channel. Please consider to be active there as well, ask your questions, etc. You can find the links for our LinkedIn group and our Twitter account on the Home page.
## How are things decided in the GSCL?
tba
## What happens in the yearly meeting of the members? Should I attend?
tba
## How can I join the GSCL?
If you plan to become a member of GSCL, please note the following: memberships must be applied for by entering the requested information into our online form. If our board does not object, our information officer will contact you usually within 2 weeks and will send a confirmation. By entering GSCL – from a legal perspective – you will become a member of a registered association for which yearly fees are to be paid with the following exception: As long as you send proof of being an immatriculated student, the membership will be free of charge (this also applies for doctoral students).
## What is the constitution ("Satzung")?
Our constitution ("Satzung") is the legally valid document describing inter alia the rules of your membership. The "Satzung" is available on our website.
## What happens with my personal data?
Your personal data entered in the abovementioned form will be used internally: your email-address is used for all electronic communication including administrative communication (e.g. invoices for membership fees) with our treasurer. Your postal address is utilized for sending you our printed newsletter appearing yearly. These data will usually be deleted one year after your membership has ended. Please, keep us informed about these data so that we can stay in touch (send information about any changes to schatzmeister@gscl.org)
## How do I make sure my student membership is free of charge?
Students must send their proof of matriculation at least for the year of their membership without being prompted. If no proof arrives at schatzmeister@gscl.org, the fees for a regular membership (currently 50 Euro) will be billed, usually in July or August. So to help us saving administrative work, please keep us informed about your status each year until the end of June or pay the regular fee without being prompted. It is always possible (and helps to reduce administrative work) to send us a filled SEPA direct debit mandate
## How can I end my GSCL membership?
In the case that you plan to end your membership, please be informed that this is only possible for the 31st December of the respective year (by sending an email to schatzmeister@gscl.org). In that case, we need to receive your termination notice before 30th September. You may become member of GSCL again anytime, but all open fees are due for three years after the end of your membership (see §195 BGB) and must be paid before re-entering the association.
