---
layout: page
title: Communication
thumbnail: "assets/img/portfolio/faq-300x225.jpg"
img: "assets/img/portfolio/faq-300x225.jpg"
date: June 27, 2021
tags: [resources]
lang: en
---


Postings related to computational linguistics, technical information, short reviews of books and software, announcements of conferences and events, inquiries, discussion suggestions, etc. can be submitted to the GSCL mailing list at any time by e-mail. In addition, GSCL uses [Twitter](https://twitter.com/gscl_org) and [LinkedIn](https://www.linkedin.com/company/german-society-for-computational-linguistics-and-language-technology-gscl/) for news items on ongoing events. GSCL members receive regular hard copies of the GSCL newsletter.

