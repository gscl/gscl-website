---
layout: page
title: Become a Member
lang: en
feature-img: "assets/img/award.jpg"
tags: [mitgliedschaft]
---


To apply for membership, send an informal email to informationsreferent@gscl.org or use our [email template](mailto:informationsreferent@gscl.org?subject=GSCL%20Membership&body=Hi%20GSCL Board%2C%0D%0A%0D%0I%20would%20like%20to%20become%20a%20member%20of%20GSCL%20.%0D%0AName%3A%0D%0ATitle%3A%0D%0AAddress%3A%0D%0AType%20(regul%C3%A4r%2C%20Student%3Ain%2C%20retired)%3A%0D%0A).


Annual membership amounts are:

| ----------- | ----------- |
| Members with regular income | 50 € |
| Pensioners | 15 € (one-time proof required) |
| People without professional income, students and enrolled PhD students | free of charge (annual proof required) |


# Benefits of membership

Members of the GSCL regularly receive information about conferences, workshops, etc. related to computational linguistics and can participate in many of these events at preferential rates.
The GSCL internal mailing list is used to announce recent publications, discuss research questions, or distribute job postings for computational linguists.
Furthermore, the GSCL uses Twitter and LinkedIn for short news updates.
The publication organ of the GSCL is the Journal for Language Technology and Computational Linguistics (JLCL).

Books of the series Sprache und Computer (ed.: P. Hellwig, J. Krause) published by Georg Olms Verlag Hildesheim are available to GSCL members at a reduced price.

# Notes
You can help us to reduce the administrative burden by giving the GSCL a direct debit authorization for the membership fees. You can use [this form]({{"/assets/GSCL-SEPA-Lastschriftmandat.pdf" || relative_url }}) (in German) to do so. Please fill it out and send it to the GSCL treasurer:

Gesellschaft für Sprachtechnologie<br/>
und Computerlinguistik GSCL e.V.<br/>
z.Hd. Gertrud Faaß<br/>
Postfach 10 05 03<br/>
31105 Hildesheim<br/><br/>

If you choose a discounted membership, please send us an appropriate proof (e.g. certificate of study, pensioner ID) within four weeks: schatzmeister@gscl.org.

Other inquiries and notices regarding your current membership, please contact GSCL Treasurer Gertrud Faaß: schatzmeister@gscl.org

