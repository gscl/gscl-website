---
layout: page
title: Kommunikation
thumbnail: "assets/img/portfolio/faq-300x225.jpg"
img: "assets/img/portfolio/faq-300x225.jpg"
tags: [resources]
lang: de
---

Beiträge mit Bezug zur Computerlinguistik, Fachinformationen, Kurzrezensionen von Büchern und Software, Veranstaltungsinformationen, Anfragen, Diskussionsanregungen etc. können jederzeit per E-Mail an die GSCL-Mailingliste eingereicht werden. Darüber hinaus nutzt die GSCL [Twitter](https://twitter.com/gscl_org) and [LinkedIn](https://www.linkedin.com/company/german-society-for-computational-linguistics-and-language-technology-gscl/) für aktuelle Kurzmeldungen. GSCL-Mitglieder erhalten regelmäßig den GSCL-Newsletter in gedruckter Form.
