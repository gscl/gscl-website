---
layout: page
title: Mitglied werden
lang: de
feature-img: "assets/img/award.jpg"
tags: [mitgliedschaft]
---


Die Beantragung der Mitgliedschaft erfolgt formlos per Email an informationsreferent@gscl.org oder per [Email-Template](mailto:informationsreferent@gscl.org?subject=GSCL%20Mitgliedschaft&body=Hallo%20GSCL-Vorstand%2C%0D%0A%0D%0Ahiermit%20m%C3%B6chte%20ich%20mein%20Interesse%20an%20einer%20Mitgliedschaft%20bei%20der%20GSCL%20zeigen.%0D%0AName%3A%0D%0AAnrede%3A%0D%0AAdresse%3A%0D%0AMitgliedschaftstyp%20(regul%C3%A4r%2C%20Student%3Ain%2C%20Renter%3Ain)%3A%0D%0A).


Die jährlichen Beträge für eine Mitgliedschaft sind:

| ----------- | ----------- |
| Menschen mit regulärem Einkommen  | 50 €  |
| Rentner:innen  | 15 € (einmaliger Nachweis erforderlich) |
| Menschen ohne Berufseinkommen, Studierende und immatrikulierte Promovierende  | kostenlos (jährlicher Nachweis erforderlich) |

# Vorteile der Mitgliedschaft

Mitglieder der GSCL erhalten regelmäßig Informationen über Tagungen, Workshops u.Ä. mit computerlinguistischem Bezug und können an vielen dieser Veranstaltungen zu Vorzugsgebühren teilnehmen.
Über die GSCL-interne Mailingliste werden aktuelle Publikationen angekündigt, Forschungsfragen diskutiert oder Stellenausschreibungen für Computerlinguisten verteilt.
Weiterhin nutzt die GSCL Twitter und LinkedIn für aktuelle Kurzmeldungen.
Publikationsorgan der GSCL ist das Journal for Language Technology and Computational Linguistics (JLCL).
Bücher der Reihe Sprache und Computer (Hrsg.: P. Hellwig, J. Krause) im Georg Olms Verlag Hildesheim können GSCL-Mitglieder preisermäßigt beziehen.

# Hinweise
Sie können uns helfen, den Verwaltungsaufwand zu verringern, indem Sie der GSCL eine Einzugsermächtigung für die Mitgliedsbeiträge erteilen. Dazu können Sie [dieses Formular]({{"/assets/GSCL-SEPA-Lastschriftmandat.pdf" || relative_url }}) verwenden. Senden Sie es bitte ausgefüllt an die Schatzmeisterin der GSCL:

**Postadresse:**

Gesellschaft für Sprachtechnologie<br/>
und Computerlinguistik GSCL e.V.<br/>
z.Hd. Gertrud Faaß<br/>
Postfach 10 05 03<br/>
31105 Hildesheim<br/>

Bei Wahl einer rabattierten Mitgliedschaft senden Sie uns bitte binnen vier Wochen einen entsprechenden Nachweis (z.B. Studienbescheinigung, Rentnerausweis) zu: schatzmeister@gscl.org. Wenn von eingetragenen studentischen Mitgliedern kein solcher Nachweis (vor Juni jeden Jahres) erfolgt, werden sie zu regulären Mitgliedern.

Sonstige Anfragen und Hinweise zu Ihrer laufenden Mitgliedschaft bitte an die GSCL-Schatzmeisterin Gertrud Faaß: schatzmeister@gscl.org
