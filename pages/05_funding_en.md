---
layout: page
title: Funding
permalink: /funding/
subtitle: 
feature-img: "assets/img/computer.jpg"
lang: en
tags: [funding]
toc: false
---

## Awards and Grants for Promoting Young Talent

The GSCL awards prizes for student theses (BA/MA) and dissertations on an annual basis.
- [GSCL doctoral dissertation award in memory of Wolfgang Hoeppner](/activities/phdaward/)
- [GSCL award for student theses](/activities/studentaward/)
* [GSCL MicroGrants](/activities/microgrants/)

## Funding for Workshops

Upon request, the GSCL offers funding for member-organized workshops, provided they comply with the [funding guidelines](vergaberichtlinienworkshops).


## Travel Stipends
The GSCL regularly issues stipends to enable students and doctoral students to enable their participation in conferences and workshops.

Current call:
- [GSCL Conference stipend for students (KONVENS 2024)](/activities/conference-stipend/)

## Computational Linguistics Fall School
In 2024, the GSCL will sponsor the [Computational Linguistics Fall School](https://cl-fallschool-2024.github.io/) for the first time together with the DGfS-CL.