---
layout: page
title: Special Interest Groups
permalink: /activities/
subtitle: 
feature-img: "assets/img/computer.jpg"
lang: en
tags: [Page]
toc: false
---



The special interest groups of GSCL serve as a platform for exchange and collaboration for interested parties from science and industry and are intended to promote the exchange of experience and knowledge transfer within the respective research network.

Special interest groups focus on specific topics from the field of computational linguistics and related research areas. Their activities include the organization of meetings, lectures and workshops as well as joint publications.

And, last but not least: Many students have presented "their" project to a larger professional audience for the first time in the setting of a SIG, thereby dipping their toes in the waters of science.

Membership in the working groups is free of charge for GSCL members.
- [Computational Linguistics for the Political and Social Sciences](politicssocialsciences)
- [Corpus Linguistics and Text Technology](texttechnology)
- [Sentiment Analysis](germansentiment)


<!--
# Portal Computational Linguistics

The web portal [Computational Linguistics](http://www.computerlinguistik.org/) provides references to CL-related institutions, projects, resources, educational opportunities, job postings, and events.

![Portal Computational Linguistics]({{ "/assets/img/cl-portal.jpg" | relative_url }}) "Portal Computational Linguistics")
-->




