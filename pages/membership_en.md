---
layout: page
title: Membership
permalink: /membership/
feature-img: "assets/img/aboutus.jpg"
lang: en
tags: [Page]
---

<div class="posts">
     {% for page in site.membership%}
    <div class="post-teaser">
        <span>
          <header>
            <h2>
              <a aria-label="{{ page.title }}" class="post-link" href="{{ page.url | relative_url }}">
                {{ page.title }}
              </a>
            </h2>
          </header>
          {% if site.excerpt or site.theme_settings.excerpt %}
              <div class="excerpt">
                <a aria-label="{{ page.title }}" class="post-link" href="{{ page.url | relative_url }}">
                  {% if site.excerpt == "truncate" %}
                     {{ page.content | strip_html | truncate: '450', '' | escape| append: site.data.language.str_truncation_continuation }}
                  {% else %}
                     {{ page.excerpt | strip_html | escape }}
                  {% endif %}
                  </a>
              </div>
          {% endif %}
      </span>
    </div>
    {% endfor %}
</div>
