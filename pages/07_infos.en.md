---
layout: page
title: Infos
permalink: /infos/
feature-img: "assets/img/aboutus.jpg"
tags: [Page]
lang: en
---


## Infos on Computational Linguistics and Natural Language Processing

Computational linguistics (CL) or linguistic data processing (LDV) investigates how natural language in text or speech data can be processed algorithmically with the help of computers. It develops the theoretical foundations of the representation, recognition and generation of spoken and written language by machines and is the __interface between linguistics and computer science__. In addition to the term Natural Language Processing (NLP), Computational Linguistics (CL) is commonly used in English-language literature and computer science. NLP focuses more on creating computer-based methods and applications for text and language processing. At the same time, CL is more concerned with acquiring knowledge about language with the help of computer-aided methods. Members of the GSCL represent the entire spectrum of these different forms of machine language processing.

A good starting point for further information on CL and NLP is the article on computational linguistics on [Wikipedia](https://en.wikipedia.org/wiki/Computational_linguistics).

We try to provide as complete an alphabetical list of study programmes and locations in Germany as possible.
If your location or study programme is not listed, please contact [us](mailto:informationsreferent@gscl.org).

## Study Computational Linguistics
*alphabetically by location*<br/>
*Site is under construction!*

### Bachelor
* [Friedrich-Alexander-Universität Erlangen](https://www.linguistik.phil.fau.de/): Computerlinguistik (B.A.)
* [Universität Heidelberg](https://www.cl.uni-heidelberg.de/): Computerlinguistik (B.A.)
* [Heinrich-Heine-Universität Düsseldorf](https://www.ling.hhu.de/bereiche-des-institutes/abteilung-fuer-computerlinguistik): Computerlinguistik (B.A.)
* [Ludwig-Maximilians-Universität München](https://www.cis.uni-muenchen.de/): Computerlinguistik (B.Sc.)
* [Universität Potsdam](https://www.uni-potsdam.de/en/ling/index): Computerlinguistik (B.Sc.)
* [Universität des Saarlandes](https://www.lst.uni-saarland.de/): Language Science (B.A.), Computerlinguistik (B.Sc.)
* [Universität Stuttgart](https://www.ims.uni-stuttgart.de/): Maschinelle Sprachverarbeitung (B.Sc.)
* [Universität Trier](https://www.uni-trier.de/universitaet/fachbereiche-faecher/fachbereich-ii/faecher/computerlinguistik-und-digital-humanities/computerlinguistik): Sprache, Technologie, Medien (B.Sc.)
* [Eberhard-Karls-Universität Tübingen](https://uni-tuebingen.de/fakultaeten/philosophische-fakultaet/fachbereiche/neuphilologie/seminar-fuer-sprachwissenschaft/): Internationaler Studiengang Computerlinguistik (B.A.)
* [Universität Zürich](https://www.cl.uzh.ch/de.html): Computerlinguistik und Sprachtechnologie (B.A.)

### Master
* [Universität Darmstadt](): Linguistic and Literary Computing (M.A.)
* [Universität Heidelberg](https://www.cl.uni-heidelberg.de/): Computerlinguistik (M.A.)
* [Ludwig-Maximilians-Universität München](https://www.cis.uni-muenchen.de/): Computerlinguistik mit Nebenfach (M.Sc.)
* [Universität des Saarlandes](https://www.lst.uni-saarland.de/): Language and Communication Technologies (M.Sc.), Language Science and Technology (M.Sc.)
* [Universität Stuttgart](https://www.ims.uni-stuttgart.de/): Computational Linguistics (M.Sc.)
* [Universität Trier](https://www.uni-trier.de/universitaet/fachbereiche-faecher/fachbereich-ii/faecher/computerlinguistik-und-digital-humanities/computerlinguistik): Natural Language Processing (M.Sc.)
* [Eberhard-Karls-Universität Tübingen](https://uni-tuebingen.de/fakultaeten/philosophische-fakultaet/fachbereiche/neuphilologie/seminar-fuer-sprachwissenschaft/): Internationaler Studiengang Computerlinguistik (M.A.)
* [Universität Zürich](https://www.cl.uzh.ch/de.html): Computational Linguistics & Language Technology (M.A.)


## Computational linguistics and NLP professorships
*alphabetically by name*<br/>
*Site is under construction!*<br/>

### Germany

#### A
* [Heike Adel-Vu](https://scholar.google.com.sg/citations?user=Fejbq9kAAAAJ&hl=en), Hochschule der Medien Stuttgart

#### B
* [Chris Biemann](https://scholar.google.com.vn/citations?user=BdwP-3QAAAAJ&hl=en), Universität Hamburg
* [Hendrik Buschmeier](https://0-scholar-google-com.brum.beds.ac.uk/citations?user=3xr2U4UAAAAJ&hl=en), Universität Bielefeld

#### C
* [Christian Chiarcos](https://scholar.google.com/citations?user=IYbsrxUAAAAJ), Universität Augsburg

#### D
* [Vera Demberg](https://scholar.google.com.sg/citations?hl=en&user=l2CFSAMAAAAJ&view_op=list_works&sortby=pubdate), Universität des Saarlandes

#### F
* [Lucie Flek](https://scholar.google.com.hk/citations?hl=en&user=qZCZFp0AAAAJ&view_op=list_works&sortby=pubdate), Universität Bonn
* [Anette Frank](https://scholar.google.com.sg/citations?hl=en&user=9FP2fokAAAAJ), Universität Heidelberg
* [Annemarie Friedrich](https://scholar.google.com.sg/citations?hl=en&user=8CVIK-UAAAAJ), Universität Augsburg
* [Alexander Fraser](https://scholar.google.com.sg/citations?hl=en&user=4ZIZK08AAAAJ), Ludwig-Maximilians-Universität München

#### G
* [Josef van Genabith](https://scholar.google.com.sg/citations?hl=en&user=rl8S6a8AAAAJ), Universität des Saarlandes
* [Munir Georges](https://scholar.google.com.sg/citations?hl=en&user=0MgJDEMAAAAJ), Technische Hochschule Ingolstadt
* [Goran Glavaš](https://scholar.google.com.sg/citations?hl=en&user=Ym0myOwAAAAJ), Universität Würzburg
* [Iryna Gurevych](https://scholar.google.com.sg/citations?hl=en&user=t3A39e8AAAAJ), Technische Universität Darmstadt

#### H
* Ulrich Heid, Universität Hildesheim

#### K
* [Dietrich Klakow](https://scholar.google.com.sg/citations?hl=en&user=_HtGYmoAAAAJ), Universität des Saarlandes
* [Roman Klinger](https://scholar.google.com.sg/citations?hl=en&user=1flvefwAAAAJ), Otto-Friedrich-Universität Bamberg
* [Alexander Koller](https://scholar.google.com.sg/citations?hl=en&user=yni3K9wAAAAJ), Universität des Saarlandes
* [Jonas Kuhn](https://scholar.google.com.sg/citations?hl=en&user=t5vqVEkAAAAJ), Universität Stuttgart

#### M
* Katja Markert, Universität Heidelberg
* [Jens Michaelis](https://scholar.google.com.sg/citations?hl=en&user=BzHkXxsAAAAJ), Universität Bielefeld
* [Margot Mieskes](https://scholar.google.com.sg/citations?hl=en&user=NSHuWowAAAAJ), Hochschule Darmstadt

#### N
* [Günter Neumann](https://scholar.google.com.sg/citations?hl=en&user=42gs8NAAAAAJ), Universität des Saarlandes / DFKI

#### P
* [Sebastian Padó](https://scholar.google.com.sg/citations?hl=en&user=vKqag_AAAAAJ), Universität Stuttgart
* Ulrike  Padó, Hochschule für Technik Stuttgart
* [Barbara Plank](https://scholar.google.com.sg/citations?hl=en&user=5nGlwIQAAAAJ), Ludwig-Maximilians-Universität München
* [Simone Ponzetto](https://scholar.google.com.sg/citations?hl=en&user=VmIFG0EAAAAJ), Universität Mannheim

#### R
* [Nils Reiter](https://scholar.google.com.sg/citations?hl=en&user=kC0dkncAAAAJ), Universität Köln
* [Stefan Riezler](https://scholar.google.com.sg/citations?hl=en&user=nY9tQLYAAAAJ), Universität Heidelberg

#### S
* [David Schlangen](https://scholar.google.com.sg/citations?hl=en&user=QoDgwZYAAAAJ), Universität Potsdam
* [Bernhard Schröder](https://scholar.google.com.sg/citations?hl=en&user=mTzlXBUAAAAJ), Universität Duisburg-Essen
* [Hinrich Schütze](https://scholar.google.com.sg/citations?hl=en&user=qIL9dWUAAAAJ), Ludwig-Maximilians-Universität München
* [Manfred Stede](https://scholar.google.com.sg/citations?hl=en&user=I1wvHnIAAAAJ), Universität Potsdam
* [Michael Strube](https://scholar.google.com.sg/citations?hl=en&user=s0_rS0kAAAAJ), HITS & Universität Heidelberg

#### U
* [Stefan Ultes](https://scholar.google.com.sg/citations?hl=en&user=WHHRFV8AAAAJ), Universität Bamberg

#### V
* Ngoc Thang Vu, Universität Stuttgart

#### W
* [Christian Wartena](https://scholar.google.com.sg/citations?hl=en&user=8l0v19EAAAAJ), Hochschule Hannover
* [Andreas Witt](https://scholar.google.com.sg/citations?hl=en&user=Y2NLolcAAAAJ), Universität Mannheim

#### Z
* [Sina Zarrieß](https://scholar.google.com.sg/citations?hl=en&user=7OOP0iAAAAAJ), Universität Bielefeld
* [Torsten Zesch](https://scholar.google.com.sg/citations?hl=en&user=vWeDs00AAAAJ), Fernuniversität Hagen
* [Heike Zinsmeister](https://scholar.google.com.sg/citations?hl=en&user=p9mPuqQAAAAJ), Universität Hamburg

#### Junior professorships / Junior research groups
* Michael Hahn, Universität des Saarlandes
* Andrea Horbach, Universität Hildesheim (Digital Humanities)
* Michael Roth, Emmy-Noether-Nachwuchsgruppe Universität Stuttgart
* Carina Silberer, Universität Stuttgart
* Gabriella Lapesa, Universität Stuttgart
* Tatjana Scheffler, Ruhr-Universität Bochum
* Anette Hautli-Janisz, Universität Passau


### Austria

* [Benjamin Roth](https://scholar.google.com.sg/citations?hl=en&user=hz5AsE0AAAAJ), Universität Wien


### Suisse

* [Mark Cieliebak](https://scholar.google.com.sg/citations?hl=en&user=yT-vIQMAAAAJ), Zürcher Hochschule der Angewandten Wissenschaften
* [Cerstin Mahlow](https://scholar.google.com.sg/citations?hl=en&user=-ZwJs6AAAAAJ), Zürcher Hochschule für Angewandte Wissenschaften
* [Rico Sennrich](https://scholar.google.com.sg/citations?hl=en&user=XTpJvCgAAAAJ), Universität Zürich
* [Martin Volk](https://scholar.google.com.sg/citations?hl=en&user=8bPj2w4AAAAJ), Universität Zürich
