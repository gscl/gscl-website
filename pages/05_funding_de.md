---
layout: page
title: Förderung
permalink: /funding/
subtitle: 
feature-img: "assets/img/computer.jpg"
lang: de
tags: [funding]
toc: false
---


## Preise und Förderung für Nachwuchswissenschaftler und Studierende
Die GSCL vergibt Auszeichnungen für studentische Abschlussarbeiten (BA/MA) und Dissertationen im jährlichen Wechsel.
* [GSCL-Promotionspreis zum Gedenken an Wolfgang Hoeppner](/activities/phdaward/)
* [GSCL-Preis für studentische Abschlussarbeiten](/activities/studentaward/)
* [GSCL MicroGrants](/activities/microgrants/)


## Förderung von Workshops
Auf Antrag kann die GSCL Fördermittel für von Mitgliedern organisierte Workshops bereitstellen, sofern diese den [Vergaberichtlinien](/activities/vergaberichtlinienworkshops) entsprechen.


## Reisestipendien
Die GSCL vergibt regelmäßig Reisestipendien, um es Studierenden und Doktorand*innen der Computerlinguistik zu ermöglichen, an Konferenzen und Workshops teilzunehmen.
- [GSCL Konferenzstipendium für Studierende (KONVENS 2024)](/activities/conference-stipend/)

## Computational Linguistics Fall School
Im Jahr 2024 sponsort die GSCL zum ersten Mal gemeinsam mit der DGfS-CL die [Computational Linguistics Fall School](https://cl-fallschool-2024.github.io/).