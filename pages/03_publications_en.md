---
layout: page
title: Publications
permalink: /publications/
subtitle: 
feature-img: "assets/img/computer.jpg"
lang: en
tags: [publications]
toc: false
---



## Conference Proceedings

The Proceedings of KONVENS and GSCL conferences will be made available collectively at a [unified address](https://konvens.org/site/proceedings). Beginning with 2021's edition, the proceedings of KONVENS have been archived in the [ACL Anthology](https://aclanthology.org/venues/konvens/).

## Journal for Language Technology and Computational Linguistics (JLCL).

The publication outlet of GSCL is the [Journal for Language Technology and Computational Linguistics (JLCL)](http://www.jlcl.org/), formerly LDV Forum. The journal publishes technical papers and reports, discussions and reviews from the entire spectrum of language technology and computational linguistics. JLCL also regularly features issues dedicated to special topics and edited by guest editors. The editors decide on the acceptance of technical contributions after (at least two) reviews by members of the scientific advisory board (editorial board).
