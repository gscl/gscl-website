---
layout: page
title: Imprint & Privacy
permalink: /imprint
lang: en
tags: [Page]
hide: true
footer: true
---

**Gesellschaft für Sprachtechnologie und Computerlinguistik e.V. (GSCL)**<br/>
E-Mail: [GSCL-Vorstand\@gscl.org](mailto:GSCL-Vorstand@gscl.org)

**Postal Address:**<br/>
Gesellschaft für Sprachtechnologie<br/>
und Computerlinguistik GSCL e.V.<br/>
Postfach 10 05 03<br/>
31105 Hildesheim<br/>
Germany<br/><br/>

**Authorized to represent:**<br/>

**President (V.i.S.d § 55 Abs. 2 RStV):**<br/>
Prof. Dr. Annemarie Friedrich<br/>
Universität Augsburg<br/>
Universitätsstr. 2<br/>
86135 Augsburg<br/><br/>

**Registergericht:** Amtsgericht Hildesheim<br/>
**Registernummer:** VR8710<br/><br/>

**Privacy**<br/>

The use of our website is generally possible without providing personal data. Data is logged about access to the website, e.g., date and time, amount of data sent in bytes, browser used, and IP address used.

This data is stored on our website as "server log files" and deleted regularly. Cookies are used to increase the user-friendliness and security of this website. You can turn off these cookies in your browser's settings options if required.  Insofar as our pages' personal data (e.g. name, address or e-mail address) are collected, this is always done voluntarily for member administration and to fulfil the association's tasks.

This data will not be passed on to third parties without your express consent. We, at this moment, expressly object to the use of contact data published within the scope of the imprint obligation by third parties to send unsolicited advertising and information material.

# [Images]

Image \"Alte Bücher\" by [Michal Jarmoluk] at [Pixabay]

Image \"Leipzig Stadt Aussicht\" by [Thomas Wolter] at [Pixabay][2]

Image \"Video Conference\" by [febrian eka saputra] at
[Pixabay](https://pixabay.com/de/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=5484678)

  [1]: #imprint-privacy {.color-inherit .text-decoration-0}
  [Images]: #images {.color-inherit .text-decoration-0}
  [Michal Jarmoluk]: https://pixabay.com/de/users/jarmoluk-143740/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=436498
  [Pixabay]: https://pixabay.com/de/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=436498
  [Thomas Wolter]: https://pixabay.com/de/users/thomaswolter-92511/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=263165
  [2]: https://pixabay.com/de/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=263165
  [febrian eka saputra]: https://pixabay.com/de/users/febrianes86-5873902/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=5484678

  Image by [Deedster] at [Pixabay]

  [Deedster]: https://pixabay.com/de/users/deedster-2541644/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=1643784
  [Pixabay]: https://pixabay.com/de/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=1643784
  
  Image by [Alexas_Fotos] at [Pixabay]

  [Alexas_Fotos]: https://pixabay.com/de/users/alexas_fotos-686414/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=3408300
  [Pixabay]: https://pixabay.com/de/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=3408300

  Image by [Наталья Коллегова] at [Pixabay]
  

  [Наталья Коллегова]: https://pixabay.com/de/users/natalia_kollegova-5226803/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=2631913
  [Pixabay]: https://pixabay.com/de/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=2631913


Image by <a href="https://pixabay.com/de/users/openclipart-vectors-30363/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=152803">OpenClipart-Vectors</a> at <a href="https://pixabay.com/de//?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=152803">Pixabay</a>

Title image by <a href="https://www.pexels.com/de-de/foto/multiethnische-studenten-die-gemeinsam-in-der-bibliothek-forschen-5940827/">Kampus Production</a>.