---
layout: page
title: Arbeitskreise
permalink: /activities/
subtitle: 
feature-img: "assets/img/computer.jpg"
lang: de
tags: [Page]
toc: false
---


Die Arbeitskreise der GSCL dienen als Kommunikations- und Arbeitsplattform für Interessierte aus Wissenschaft und Industrie und sollen Erfahrungsaustausch und Wissenstransfer innerhalb des jeweiligen Forschungsnetzwerks befördern.
Arbeitskreise befassen sich gezielt mit einzelnen Schwerpunkten aus dem Themenspektrum der Computerlinguistik und angrenzender Forschungsgebiete.
In diesem Zusammenhang werden Arbeitstreffen, Vorträge und Workshops organisiert sowie gemeinsame Publikationen erarbeitet.
Und, nicht zuletzt: Viele Studierende haben in diesem Rahmen erstmals "ihr" Projekt einem größeren Fachpublikum vorgestellt und damit ihre ersten Schritte auf dem wissenschaftlichen Podium getan.
Die Mitgliedschaft in den Arbeitskreisen ist für GSCL-Mitglieder gebührenfrei.
- [Computerlinguistik für die Politik- und Sozialwissenschaften](politicssocialsciences)
- [Korpuslinguistik und Texttechnologie](texttechnology)
- [Stimmungsanalyse](germansentiment)




