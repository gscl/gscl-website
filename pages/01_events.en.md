---
layout: page
title: Events
permalink: /events/
feature-img: "assets/img/events.jpg"
lang: en
tags: [Page]
---

Conferences and events (co-)organised by the GSCL with the aim of fostering the exchange of experiences on a high level by presenting foundational research in computational linguistics and selected practical talks by experts. These conferences and events also serve as a platform for presenting excellent junior research(ers) and the GSCL Awards for [PhD](/activities/phdaward) and [BA/MA](/activities/studentaward) theses.

<!-- GSCL Research and Tutorial Talks

Virtual events were held regularly from January 2021 to December 2022 with the aim of regular technical and linguistic exchange. There were two formats: "Research Talks" in the form of invited lectures on current research topics and "Tutorial Talks" with the primary target group of students and doctoral candidates.
The Research and Tutorial Talks were discontinued for the time being starting January 2023.
A list of the talks held to date can be found [here](gscltalksarchive/).-->


# KONVENS

[KONVENS](https://www.konvens.org) started as a co-ordinated event of several professional organisations from the German-speaking area.  Since 2018, [KONVENS](https://www.konvens.org) has been the overarching conference on computational linguistics in the German-speaking area.

* [KONVENS 2024 in Wien](https://konvens-2024.univie.ac.at/)
* [KONVENS 2023 in Ingolstadt](https://www.thi.de/konvens-2023/) (Proceedings: [Proceedings](https://aclanthology.org/events/konvens-2022/))
* [KONVENS 2022 in Potsdam](https://konvens2022.uni-potsdam.de)
* [KONVENS 2021 in Düsseldorf](https://konvens2021.phil.hhu.de/) (Proceedings: [Main](https://aclanthology.org/events/konvens-2021/), [Workshops](https://konvens.org/proceedings/2021/index.html))
* [KONVENS 2020 in Zürich](https://swisstext-and-konvens-2020.org/)
* [KONVENS 2019 in Erlangen](https://2019.konvens.org/) ([Proceedings](https://konvens.org/proceedings/2019/index.html))
* [KONVENS 2018 in Wien](https://www.oeaw.ac.at/ac/konvens2018/)  ([Proceedings](https://konvens.org/proceedings/2018/index.html))
* [KONVENS 2016 in Bochum](https://www.linguistics.rub.de/konvens16/)  ([Proceedings](https://konvens.org/proceedings/2016/index.html))
* [KONVENS 2014 in Hildesheim](http://www.uni-hildesheim.de/konvens2014/) ([Proceedings](https://hildok.bsz-bw.de/solrsearch/index/search/searchtype/collection/id/16222))
* [KONVENS 2012 in Wien](http://www.oegai.at/konvens2012/)  ([Proceedings](https://konvens.org/proceedings/2012/index.html))
* [KONVENS 2010 in Saarbrücken](http://konvens2010.coli.uni-saarland.de/) ([Proceedings](http://universaar.uni-saarland.de/monographien/volltexte/2010/12/pdf/konvens_2010.pdf))
* [KONVENS 2008 in Berlin](http://www.wikicfp.com/cfp/servlet/event.showcfp?eventid=2482&copyownerid=706)
* [KONVENS 2006 in Konstanz](http://ling.uni-konstanz.de/pages/conferences/konvens06/)
* KONVENS 2004 in Wien
* [KONVENS 2002 in Saarbrücken](http://konvens2002.dfki.de/)  ([Proceedings](https://konvens.org/proceedings/2002/index.html))
* [KONVENS 2000 in Ilmenau](https://dblp.uni-trier.de/db/conf/konvens/konvens2000.html)
* KONVENS 1998 in Bonn
* [KONVENS 1996 in Bielefeld](https://dblp.uni-trier.de/db/conf/konvens/konvens1996.html)
* [KONVENS 1994 in Wien](http://www.oegai.at/konvens94.shtml)
* [KONVENS 1992 in Nürnberg](https://dblp.uni-trier.de/db/conf/konvens/konvens1992.html)

# TaCoS

* [TaCoS](https://linguistik.computer) is a conference series dating back to the 1990s that is open to any student interested in the areas of computational linguistics and natural language processing as well as its neighboring applied and theoretical disciplines from psycholinguistics to computer science.

# Workshops

* [KONVENS Teach4NLP 2023](/events/teach4nlp2023): KONVENS Teaching for NLP Workshop
* [KONVENS CPSS 2023](/events/cpss2023konvens): KONVENS CPSS Workshop
* [Ethics-Workshop 2019](/events/ethics)



## GSCL/GLDV-Conferences (every two years until 2017)

Since 2018, the conference series has been merged with KONVENS.

* [GSCL Conference 2017 in Berlin](http://gscl2017.dfki.de/)  ([Proceedings](https://konvens.org/proceedings/2017/index.html))
* [GSCL Conference 2015 in Duisburg/Essen](https://konvens.org/proceedings/2015/index.html)
* [GSCL Conference 2013 in Darmstadt](http://gscl2013.ukp.informatik.tu-darmstadt.de/) ([Proceedings](https://www.springer.com/gp/book/9783642407215))
* [GSCL Conference 2011 in Hamburg](http://exmaralda.org/gscl2011/downloads/AZM96.pdf)
* [GSCL Conference 2009 in Potsdam](https://elibrary.narr.digital/book/99.125005/9783823375111)
* [GLDV Conference 2007 in Tübingen](http://www.sfb441.uni-tuebingen.de/gldv2007/)  ([Proceedings](https://www.narr.de/datenstrukturen-fur-linguistische-ressourcen-und-ihre-anwendungen-16314))
* GLDV Conference 2005 in Bonn
* [GLDV Conference 2003 in Köthen](http://www.informatik.uni-trier.de/~ley/db/journals/ldvf/ldvf18.html)
* [GSCL Conference 2001 in Gießen](http://www.uni-giessen.de/germanistik/ascl/gldv2001/)
* [GLDV Conference 1999 in Frankfurt](http://titus.uni-frankfurt.de/curric/gldv99.htm)
* [GLDV Conference 1997 in Leipzig](https://dblp.uni-trier.de/db/conf/gldv/gldv1997.html))
* [GLDV Conference 1995 in Regensburg](https://dblp.uni-trier.de/db/conf/gldv/gldv1995.html))
* [GLDV Conference 1993 in Kiel](https://dblp.uni-trier.de/db/conf/gldv/gldv1993.html))
* GLDV Conference 1991 in Trier
* [GLDV Conference 1990 in Siegen](http://www.informatik.uni-trier.de/~ley/db/conf/gldv/gldv1990.html)
* [GLDV Conference 1989 in Ulm](http://www.informatik.uni-trier.de/~ley/db/conf/gldv/gldv1989.html)
* GLDV Conference 1988 in Saarbrücken
* [GLDV Conference 1987 in Bonn](http://www.informatik.uni-trier.de/~ley/db/conf/gldv/gldv1987.html)
* [GLDV Conference 1986 in Göttingen](http://www.informatik.uni-trier.de/~ley/db/conf/gldv/gldv1986.html)
* GLDV Conference 1985 in Hannover
* GLDV Conference 1984 in Heidelberg
* [GLDV Conference 1983 in Trier](http://www.informatik.uni-trier.de/~ley/db/conf/gldv/gldv1983.html)
* GLDV Conference 1982 in Koblenz
* GLDV Conference 1980 in Saarbrücken
* GLDV Conference 1979 in Bonn
* GLDV Conference 1978 in Essen
* GLDV Conference 1976 in München
