---
layout: page
title: About us
permalink: /about/
feature-img: "assets/img/aboutus.jpg"
lang: en
tags: [Page]
---

The German Society for Computational Linguistics and Language Technology (GSCL) is the scientific professional association in German-speaking countries and regions for machine language processing in research, teaching and the profession. It actively endeavours to forge links between universities and industry. It supports cooperation with neighbouring disciplines such as linguistics and semiotics, computer science and mathematics, psychology and cognitive science, and information and documentation science. It also maintains contacts with relevant professional associations.


# About us
___

The GSCL is the scientific association for research, teaching and professional work on natural language processing. It registered as a non-profit association, the charter can be found [here]({{ "/assets/satzung.pdf" | relative_url }}). The society represents the interests of its members and promotes cooperation between the members and their working fields, such as:

* automatic NL analysis and generation
* corpus engineering and corpus linguistics
* document processing and text technology
* information retrieval and knowledge management
* multimedia and hypermedia
* dictionaries and terminological databases
* machine translation
* human-machine communication
* artificial intelligence and machine learning
* tools for literary and linguistic research

There is a continuum between research and practical application. The GSCL actively endeavours to establish links between universities and industry. Outstanding student theses and dissertations are regularly honoured with the [GSCL Prize](/activities/studentaward/) or the [Dissertation Prize](/activities/phdaward/).

The GSCL also supports [shared task initiatives](http://www.computerlinguistik.org/portal/portal.html?s=Shared%20Tasks) like [GermEval](https://germeval.github.io/), the cooperation with neighbouring disciplines (e.g., linguistics and semiotics, computer science and mathematics, psychology and cognitive science, data and information science, digital humanities), and keeps contact to the respective associations. There are international contacts with organisations like the [Association for Computational Linguistics](https://www.aclweb.org/portal/) and the [European Association for Digital Humanities](https://eadh.org/) (EADH), formerly Association for Literary and Linguistic Computing (ALLC).



# GSCL Executive Board
___

The German Society for Language Technology and Computational Linguistics acts through its honorary mandate holders. The executive committee is responsible for all matters of the GSCL – e.g. convocation of the general meeting and implementation of its decisions, preparation of a budget, representation of GSCL to the outside – unless they are assigned by the statute to another body.

{% include board_en.html %}


# GSCL Advisory Board
___

The Advisory Board has the task of advising and assisting the Board in all matters relating to the work and tasks of the Association, including budgetary matters.


{% include boardadvisors_de.html %}



{% include about.html %}
