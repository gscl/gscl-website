---
layout: page
title: Impressum & Datenschutz
permalink: /imprint
lang: de
tags: [Page]
hide: true
footer: true
---

**Gesellschaft für Sprachtechnologie und Computerlinguistik e.V. (GSCL)**
E-Mail: [GSCL-Vorstand@gscl.org](mailto:GSCL-Vorstand@gscl.org)

**Postadresse:**<br/>
Gesellschaft für Sprachtechnologie<br/>
und Computerlinguistik GSCL e.V.<br/>
Postfach 10 05 03<br/>
31105 Hildesheim<br/><br/>

**Vertretungsberechtigt:**<br/>

**Erste Vorsitzende (V.i.S.d § 55 Abs. 2 RStV):**<br/>
Prof. Dr. Annemarie Friedrich<br/>
Universität Augsburg<br/>
Universitätsstr. 2<br/>
86135 Augsburg<br/><br/>

**Registergericht:** Amtsgericht Hildesheim<br/>
**Registernummer:** VR8710<br/><br/>



**Datenschutz**

Die Nutzung unserer Website ist in der Regel ohne Angabe
personenbezogener Daten möglich. Protokolliert werden Daten über
Zugriffe auf die Website, also z.B. Datum und Uhrzeit, Menge der
gesendeten Daten in Byte, verwendeter Browser, verwendete IP-Adresse.

Diese Daten werden auf unserer Website als \"Server-Logfiles\"
gespeichert und regelmäßig gelöscht. Zur Erhöhung der
Benutzerfreundlichkeit und Sicherheit dieser Website kommen Cookies zum
Einsatz. Sie können in den Einstellungsoptionen Ihres Browser bei Bedarf
die Verwendung dieser Cookies deaktivieren. Soweit auf unseren Seiten
personenbezogene Daten (beispielsweise Name, Anschrift oder
E-Mail-Adresse) erhoben werden, erfolgt dies stets auf freiwilliger
Basis, zur Mitgliederverwaltung und zur Duchführung der Vereinsaufgaben.

Diese Daten werden ohne Ihre ausdrückliche Zustimmung nicht an Dritte
weitergegeben. Der Nutzung von im Rahmen der Impressumspflicht
veröffentlichten Kontaktdaten durch Dritte zur Übersendung von nicht
ausdrücklich angeforderter Werbung und Informationsmaterialien wird
hiermit ausdrücklich widersprochen.

# [Images]

Bild \"Alte Bücher\" von [Michal Jarmoluk] auf [Pixabay]

Bild \"Leipzig Stadt Aussicht\" von [Thomas Wolter] auf [Pixabay][2]

Bild \"Video Conference\" von [febrian eka saputra] auf
[Pixabay](https://pixabay.com/de/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=5484678)

  [1]: #impressum-datenschutz {.color-inherit .text-decoration-0}
  [Images]: #images {.color-inherit .text-decoration-0}
  [Michal Jarmoluk]: https://pixabay.com/de/users/jarmoluk-143740/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=436498
  [Pixabay]: https://pixabay.com/de/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=436498
  [Thomas Wolter]: https://pixabay.com/de/users/thomaswolter-92511/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=263165
  [2]: https://pixabay.com/de/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=263165
  [febrian eka saputra]: https://pixabay.com/de/users/febrianes86-5873902/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=5484678

Bild von [Deedster] auf [Pixabay]

  [Deedster]: https://pixabay.com/de/users/deedster-2541644/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=1643784
  [Pixabay]: https://pixabay.com/de/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=1643784
  
  Bild von [Alexas_Fotos] auf [Pixabay]

  [Alexas_Fotos]: https://pixabay.com/de/users/alexas_fotos-686414/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=3408300
  [Pixabay]: https://pixabay.com/de/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=3408300

  Bild von [Наталья Коллегова] auf [Pixabay]
  

  [Наталья Коллегова]: https://pixabay.com/de/users/natalia_kollegova-5226803/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=2631913
  [Pixabay]: https://pixabay.com/de/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=2631913

Bild von <a href="https://pixabay.com/de/users/openclipart-vectors-30363/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=152803">OpenClipart-Vectors</a> auf <a href="https://pixabay.com/de//?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=152803">Pixabay</a>

Titelbild von <a href="https://www.pexels.com/de-de/foto/multiethnische-studenten-die-gemeinsam-in-der-bibliothek-forschen-5940827/">Kampus Production</a>.