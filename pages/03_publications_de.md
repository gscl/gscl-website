---
layout: page
title: Publikationen
permalink: /publications/
subtitle: 
feature-img: "assets/img/computer.jpg"
lang: de
tags: [publications]
toc: false
---

## Konferenz-Proceedings

Proceedings der KONVENS und der GSCL-Konferenzen werden gesammelt unter einer [einheitlichen Adresse](https://konvens.org/site/proceedings) zur Verfügung gestellt. Ab dem Jahr 2021 werden die KONVENS Proceedings in der [ACL Anthology](https://aclanthology.org/venues/konvens/) archiviert.

## Journal for Language Technology and Computational Linguistics (JLCL)

Publikationsorgan der GSCL ist das [Journal for Language Technology and Computational Linguistics (JLCL)](http://www.jlcl.org/), ehemals LDV-Forum. Die Zeitschrift veröffentlicht Fachbeiträge und Berichte, Diskussionen und Rezensionen aus dem gesamten Spektrum der Sprachtechnologie und Computerlinguistik; einzelne Hefte erscheinen dabei zu besonderen Themenschwerpunkten. Über die Annahme von Fachbeiträgen entscheiden die Herausgeber nach (mindestens zwei) Gutachten von Mitgliedern des wissenschaftlichen Beirats (Herausgebergremium).

