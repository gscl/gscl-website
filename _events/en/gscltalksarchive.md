---
layout: page
title: GSCL Talks Archive
feature-img: "assets/img/portfolio/micro-300x225.png"
img: "assets/img/portfolio/micro-300x225.png"
date: 27. Juni 2021
tags: [GSCL Talks]
lang: en
---
## GSCL Research Talks

The GSCL Research Talks, which took place between February 2021 and December 2022, provided a forum for dialogue on current research topics in academia and industry. The speakers included high-ranking researchers from academia and industry. Presentations were given in German or English, with the language of each presentation announced in advance. The event lasted one hour, including 30 minutes of questions and answers.


## GSCL Tutorial Talks

The 90-minute GSCL Tutorial Talks covered special topics in computational linguistics not typically included in the curriculum, insights into research projects or introductory lectures in neighbouring disciplines. The series also included interviews with experts about their personal experiences and presentations from industrial research. The GSCL Tutorial Talks were a forum for students and doctoral candidates to exchange ideas with experts, find out about internship or employment opportunities and network with experts and fellow students from other universities.

---
{% include gscltalks.html %}
