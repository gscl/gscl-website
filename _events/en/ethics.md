---
layout: page
title: Ethics in NLP
feature-img: "assets/img/sigs/ethics.jpg"
date: 27. Juni 2021
tags: [Ethik]
lang: en
---


As a professional society, we regularly discuss the topic of ethics in speech and text processing.
The results of the discussions and workshops are documented on this page.

* [GermEval: Questionnaire for the Preparation of Shared Tasks](../germeval)
* [Crash-Course on Ethics in NLP]({{site.baseurl}}/en/resources/ethics-crash-course)

We also offer to support students and doctoral students in particular in the ethical consideration of their work. Just send us an email!

[Contact us if you have any questions about ethics in language and text processing!](mailto:vorsitzende@gscl.org)