---
layout: page
title: Ethik in der Sprach- und Textverarbeitung
feature-img: "assets/img/sigs/ethics.jpg"
date: 27. Juni 2021
tags: [Ethik]
lang: de
---

Als Fachgesellschaft stehen setzen wir uns regelmäßig mit dem Thema Ethik in der Sprach- und Textverarbeitung auseinander.
Die Ergebnisse der Dikussionen und Workshops sind auf dieser Seite dokumentiert.

* [GermEval: Fragebogen für die Vorbereitung von Shared Tasks](../germeval)
* [Crash-Kurs Ethik in der Sprach- und Textverarbeitung]({{site.baseurl}}/resources/ethics-crash-course)


Wir bieten außerdem an, insbesondere Studierende und Doktoranden in der ethischen Betrachtung ihrer Arbeiten zu unterstützen. Schreibt uns einfach eine Mail!

[Kontakt bei Fragen zum Thema Ethik in der Sprach- und Textverarbeitung](mailto:vorsitzende@gscl.org)