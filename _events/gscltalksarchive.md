---
layout: page
title: GSCL Talks Archiv
feature-img: "assets/img/portfolio/micro-300x255.jpg"
img: "assets/img/portfolio/micro-300x255.jpg"
date: 27. Juni 2021
tags: [GSCL Talks]
lang: de
---
## GSCL Research Talks

Die GSCL Research Talks, die zwischen Februar 2021 und Dezember 2022 stattfanden, boten ein Forum für den Austausch über aktuelle Forschungsthemen in der Wissenschaft und der Industrie. Zu den Vortragenden zählten hochrangige Forscher aus Wissenschaft und Industrie. Die Vorträge wurden auf Deutsch oder Englisch gehalten, wobei die Sprache jedes Vortrags im Voraus bekannt gegeben wurde. Die Veranstaltungsdauer betrug eine Stunde, einschließlich 30 Minuten Fragen und Antworten.

## GSCL Tutorial Talks

Die 90minütigen GSCL Tutorial Talks behandelten spezielle Themen der Computerlinguistik, die normalerweise nicht im Lehrplan enthalten sind, Einblicke in Forschungsprojekte oder Einführungsvorträge in benachbarte Disziplinen. Die Reihe enthielt auch Interviews mit Experten über ihre persönlichen Erfahrungen und Präsentationen aus der industriellen Forschung. Die GSCL Tutorial Talks waren ein Forum für Studierende und Doktoranden, um sich mit Experten auszutauschen, sich über Praktikums- oder Einstellungsmöglichkeiten zu informieren und sich mit Experten und Kommilitonen anderer Universitäten zu vernetzen.

---
{% include gscltalks.html %}
